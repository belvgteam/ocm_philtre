<?php

try {
    $installer = $this;
    $installer->startSetup();
    
    $contentCol1 = <<<EOD
<div><img src="{{skin url="images/sample_banner.png"}}" alt="Sample banner" /></div>
EOD;
    $contentCol2 = <<<EOD
        <div><img src="{{skin url="images/sample_banner.png"}}" alt="Sample banner" /></div>
EOD;
    
    $_arrs = array('Footer - row 2 column 1' => $contentCol1,'Footer - row 2 column 2' => $contentCol2);
    $_count = 1;
    foreach($_arrs as $_title => $_content){
        $_id = 'block_footer_row2_column'.$_count;
        $staticBlock = array(
            'title' => $_title,
            'identifier' => $_id,
            'content' => $_content,
            'is_active' => 1,
            'stores' => array(0)
        );
        $block = Mage::getModel('cms/block')->load($_id);
        if (!$block->getId()) {
            Mage::getModel('cms/block')->setData($staticBlock)->save();
        } else {
            $block->setContent($_content)->save();
        }
        $_count++;
    }
    
    for($_i = 3; $_i < 5; $_i++){
        $_id = 'block_footer_row2_column'.$_count;
        $block = Mage::getModel('cms/block')->load($_id);
        if ($block->getId()) {
            $block->setIsActive('0')->save();
        }
    }
    

    $config = new Mage_Core_Model_Config();
    $config->saveConfig('design/footer/copyright', 'Copyright &copy; 2013 Philtre, LLC.USA, Inc. All Rights Reserved.' , 'default', 0);

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}