<?php

try {
    $installer = $this;
    $installer->startSetup();

    $contentCol1 = <<<EOD
<ul class="f-certification-left">
<li><img src="{{skin url='images/footer-logos/compact_signer BW.gif'}}" alt="Safe Cosmetics" /></li>
<li><img src="{{skin url='images/footer-logos/ewg_logo_150w.gif'}}" alt="Environmental Working Group" /></li>
<li><img src="{{skin url='images/footer-logos/cfi-footer-logo.png'}}" alt="Cruetly Free" /></li>
</ul>
EOD;
    $contentCol2 = <<<EOD
<ul class="f-certification-right">
<li><img src="{{skin url='images/footer-logos/SSLgeneric.jpg'}}" alt="SSL Certificates" /></li>
<li><img src="{{skin url='images/footer-logos/auth.net-logo.jpg'}}" alt="Authorize.Net" /></li>
<li><img src="{{skin url='images/footer-logos/chasepaytech_logo-BW.png'}}" alt="Chase Paymentech" /></li>
<li><img src="{{skin url='images/footer-logos/BuySafe.jpg'}}" alt="Buy Safe" /></li>
</ul>
EOD;

    $_arrs = array('Footer - row 2 column 1' => $contentCol1, 'Footer - row 2 column 2' => $contentCol2);
    $_count = 1;
    foreach ($_arrs as $_title => $_content) {
        $_id = 'block_footer_row2_column' . $_count;
        $staticBlock = array(
            'title' => $_title,
            'identifier' => $_id,
            'content' => $_content,
            'is_active' => 1,
            'stores' => array(0)
        );
        $block = Mage::getModel('cms/block')->load($_id);
        if (!$block->getId()) {
            Mage::getModel('cms/block')->setData($staticBlock)->save();
        } else {
            $block->setContent($_content)->save();
        }
        $_count++;
    }

    for ($_i = 3; $_i < 5; $_i++) {
        $_id = 'block_footer_row2_column' . $_count;
        $block = Mage::getModel('cms/block')->load($_id);
        if ($block->getId()) {
            $block->setIsActive('0')->save();
        }
    }

    $contentCol4 = <<<EOD
<div class="feature indent first">
<h2>LET'S CONNECT</h2>
<div class="footer-social">
<a href="www.facebook.com/philtreUSA"><img style="padding-left: 17px;" src="{{skin url="images/social/facebook_bottom.png"}}" alt="" /> </a>
<a href="www.twitter.com/philtreUSA"><img src="{{skin url="images/social/twitter_bottom.png"}}" alt="" /> </a>
<a href="www.pinterest.com/philtreUSA"><img src="{{skin url="images/social/pinterest_bottom.png"}}" alt="" /></a></div>
</div>
<p>{{block type="newsletter/subscribe" name="newsletter_footer" as="newsletter_footer" template="page/html/subscribe_footer.phtml"/}}</p>
EOD;
    $staticBlock = array(
        'title' => 'Footer column 4',
        'identifier' => 'block_footer_column4',
        'content' => $contentCol4,
        'is_active' => 1,
        'stores' => array(0)
    );
    $block = Mage::getModel('cms/block')->load('block_footer_column4');
    if (!$block->getId()) {
        Mage::getModel('cms/block')->setData($staticBlock)->save();
    } else {
        $block->setContent($contentCol4)->save();
    }
    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
?>

