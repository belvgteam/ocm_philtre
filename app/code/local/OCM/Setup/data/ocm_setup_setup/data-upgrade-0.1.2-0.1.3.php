<?php

try {
    $installer = $this;
    $installer->startSetup();
    $content = <<<EOD
<ul class="links">
<li><a title="Log In" href="{{store url="customer/account/login"}}">SIGN IN OR SIGN UP</a></li>
<li class="first"><a title="Customer Account" href="{{store url="customer/account"}}">MY ACCOUNT</a></li>
<li class="last">
<div class="social-links"><a class="first" title="Join us on Facebook" href="#"> <img src="{{skin url="images/social/facebook.png"}}" alt="facebook" /> </a> <a title="Follow us on Twitter" href="#"> <img src="{{skin url="images/social/twitter.png"}}" alt="twitter" /> </a> <a title="Pinterest" href="#"> <img src="{{skin url="images/social/pinterest.png"}}" alt="pinterest" /> </a></div>
</li>
</ul>
EOD;
    $staticBlock = array(
        'title' => 'Top Links',
        'identifier' => 'block_header_top_links',
        'content' => $content,
        'is_active' => 1,
        'stores' => array(0)
    );
    $block = Mage::getModel('cms/block')->load('block_header_top_links');
    if (!$block->getId()) {
        Mage::getModel('cms/block')->setData($staticBlock)->save();
    } else {
        $block->setData($staticBlock)->save();
    }

    $config = new Mage_Core_Model_Config();
    $config->saveConfig('ultimo/magento_blocks/top_links', '0' , 'default', 0);

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}