<?php

try {
    $installer = $this;
    $installer->startSetup();
    $contentCol1 = <<<EOD
        <div class="feature first last">
            <ul class="footer-col-1">
                <li><h2><a href="#">CUSTOMER CARE</a></h2></li>
                <li><h2><a href="{{store direct_url='customer/account'}}">MY ACCOUNT</a></h2></li>
                <li><h2><a href="{{store direct_url='about-us'}}">ABOUT PHILTRE</a></h2></li>
            </ul>
        </div>
EOD;
    $contentCol2 = <<<EOD
        <div class="feature indent first">
            <div class="footer-col-2">
                <h2><a href="#">SHOPPING HELP</a></h2>
                <h2>LEARNNING LOUNGE</h2>
                <ul>
                    <li><a href="#">Learning Spotlight</a></li>
                    <li><a href="#">Guides &amp; Reference</a></li>
                    <li><a href="#">Glossary</a></li>
                </ul>
            </div>
        </div>
EOD;
    $contentCol3 = <<<EOD
        <div class="footer-col-3">
            <h2>PHILTRE MAGAZINE</h2>
            <p>Placeholder copy: Healthy, ecoluxe grooming, beauty and living for men, women, family, community.</p>
        </div>
EOD;
    $contentCol4 = <<<EOD
        <div class="feature indent first">
            <h2>LET'S CONNECT</h2>
            <div class="icon">
                <img src="{{media url="wysiwyg/infortis/ultimo/icons/skype.png"}}" alt="" />
                <img src="{{media url="wysiwyg/infortis/ultimo/icons/skype.png"}}" alt="" />
                <img src="{{media url="wysiwyg/infortis/ultimo/icons/skype.png"}}" alt="" />
            </div>
        </div>
EOD;
    $_arrs = array('Footer column 1' => $contentCol1,'Footer column 2' => $contentCol2,'Footer column 3' => $contentCol3,'Footer column 4' => $contentCol4);
    $_count = 0;
    foreach($_arrs as $_title => $_content){
        $_count++;
        $_id = 'block_footer_column'.$_count;
        $staticBlock = array(
            'title' => $_title,
            'identifier' => $_id,
            'content' => $_content,
            'is_active' => 1,
            'stores' => array(0)
        );
        $block = Mage::getModel('cms/block')->load($_id);
        if (!$block->getId()) {
            Mage::getModel('cms/block')->setData($staticBlock)->save();
        } else {
            $block->setContent($_content)->save();
        }
    }
    

    $config = new Mage_Core_Model_Config();
    $config->saveConfig('design/footer/copyright', 'Copyright &copy; 2013 Philtre, LLC.All Rights Reserved.' , 'default', 0);

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}