<?php

try {
    $installer = $this;
    $installer->startSetup();

    $_content = <<<EOD
<div class="feature indent first">
<h2>LET'S CONNECT</h2>
<div><a href="#"><img src="{{skin url="images/social/facebook_bottom.png"}}" alt="" /> </a><a href="#"><img src="{{skin url="images/social/twitter_bottom.png"}}" alt="" /> </a><a href="#"><img src="{{skin url="images/social/pinterest_bottom.png"}}" alt="" /></a></div>
</div>
<p>{{block type="newsletter/subscribe" name="newsletter_footer" as="newsletter_footer" template="page/html/subscribe_footer.phtml"/}}</p>
EOD;
    $staticBlock = array(
        'title' => 'Footer column 4',
        'identifier' => 'block_footer_column4',
        'content' => $_content,
        'is_active' => 1,
        'stores' => array(0)
    );
    $block = Mage::getModel('cms/block')->load('block_footer_column4');
    if (!$block->getId()) {
        Mage::getModel('cms/block')->setData($staticBlock)->save();
    } else {
        $block->setContent($_content)->save();
    }

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}