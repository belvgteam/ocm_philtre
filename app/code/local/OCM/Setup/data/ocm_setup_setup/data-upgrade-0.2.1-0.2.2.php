<?php

try {
    $installer = $this;
    $installer->startSetup();

    //Create static block block_slideshow_banners
    
    $content = <<<EOD
<div class="small-banner">
<div class="top-small-banner haft-banner">
<div class="left">
<h2 class="title">Lorem Ipsum is simply</h2>
<p>dummy text of the printing and typesetting industry. Lorem Ipsum has</p>
</div>
<img src="{{skin url="images/slideshow/small1.jpg"}}" alt="Sample slide" /></div>
<div class="bottom-small-banner haft-banner">
<div class="left">
<h2 class="title">Lorem Ipsum is simply</h2>
<p>dummy text of the printing and typesetting industry. Lorem Ipsum has</p>
</div>
<img src="{{skin url="images/slideshow/small2.jpg"}}" alt="Sample slide" /></div>
</div>
EOD;
    $_static = array(
        "title"=>"Slideshow banners",
        "identifier"=>"block_slideshow_banners",
        "content"=>$content,
        "is_active"=>1,
        "stores"=>array(0)
    );
    $_block = Mage::getModel('cms/block')->load("block_slideshow_banners");
    if($_block && $_block->getId()){
        $_block->setContent($content)->save();
    } else {
        $_block->setData($_static)->save();
    }

    //Create static block philtre_home_slider

    $content = <<<EOD
<ul class="slides">
<li><a href="{{store url='#'}}"> <img src="{{skin url='images/slideshow/01.jpg'}}" alt="Sample slide" /></a>
<div class="caption dark3">
<h2 class="heading permanent"><a href="#">Sample Caption ? <span style="float: right;">&gt;&gt;</span></a></h2>
</div>
</li>
<li><a href="{{store url='#'}}"> <img src="{{skin url='images/slideshow/02.jpg'}}" alt="Sample slide" /></a>
<div class="caption dark3">
<h2 class="heading permanent"><a href="#">Sample Caption ? <span style="float: right;">&gt;&gt;</span></a></h2>
</div>
</li>
<li><a href="{{store url='#'}}"> <img src="{{skin url="images/slideshow/03.jpg"}}" alt="Sample slide" /></a>
<div class="caption dark3">
<h2 class="heading permanent"><a href="#">Sample Caption ? <span style="float: right;">&gt;&gt;</span></a></h2>
</div>
</li>
</ul>
EOD;
    $_static = array(
        "title"=>"philtre_home_slider",
        "identifier"=>"philtre_home_slider",
        "content"=>$content,
        "is_active"=>1,
        "stores"=>array(0)
    );
    $_block = Mage::getModel('cms/block')->load("philtre_home_slider");
    if($_block && $_block->getId()){
        $_block->setContent($content)->save();
    } else {
        $_block->setData($_static)->save();
    }
    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
?>

