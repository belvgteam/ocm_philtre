<?php

try {
    $installer = $this;
    $installer->startSetup();

    $contentCol2 = <<<EOD
        <div class="feature indent first">
            <div class="footer-col-2">
                <h2><a href="#">SHOPPING HELP</a></h2>
                <h2>LEARNNING LOUNGE</h2>
                <ul>
                    <li><a href="#">Learning Spotlight</a></li>
                    <li><a href="#">Guides &amp; Reference</a></li>
                    <li><a href="#">Glossary</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div>
        </div>
EOD;

    $_id = 'block_footer_column2';
    $block = Mage::getModel('cms/block')->load($_id);
    if ($block->getId()) {
        $block->setContent($contentCol2)->save();
    }

    $installer->endSetup();
} catch (Exception $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}