<?php

try {
    $installer = $this;
    $installer->startSetup();
    
    // Create CMS Page - Terms of Use
    $_content = <<<EOD
Coming Soon...
EOD;
    $cmsPage = array(
                'title' => 'Terms of Use',
                'identifier' => 'terms-of-use',
                'content' => $_content,
                'is_active' => 1,
                'sort_order' => 0,
                'stores' => array(0),
                'root_template' => 'one_column'
            );
    $_cmsPage = Mage::getModel('cms/page')->load('terms-of-use');
            if(!$_cmsPage->getId()){
                Mage::getModel('cms/page')->setData($cmsPage)->save();
            }else{
                $_cmsPage->setContent($_content);
            }

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}