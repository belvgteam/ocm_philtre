<?php

try {
    $installer = $this;
    $installer->startSetup();

    $staticBlock = array(
        'title' => 'Free Shipping',
        'identifier' => 'free-shipping',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus interdum mattis lacinia. In ac massa nec tellus posuere laoreet. Aliquam erat volutpat. Praesent non tellus eget nibh laoreet malesuada. Nam magna odio, bibendum vel tortor vitae, fringilla porttitor tortor.',
        'is_active' => 1,
        'stores' => array(0)
    );
    $block = Mage::getModel('cms/block')->load('free-shipping');
    if (!$block->getId()) {
        Mage::getModel('cms/block')->setData($staticBlock)->save();
    } else {
        $block->setContent($content)->save();
    }
    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}