<?php

$installer = $this;
$installer->startSetup();
$_topCategory = array(
    "Men"=>array('position'=>0,'subcategory'=>'Man'),
    "Skincare"=>array('position'=>0,'subcategory'=>'Mommy'),
    "Bath & Body"=>array('position'=>1,'subcategory'=>'Fur Baby'),
    "Haircare"=>array('position'=>2,'subcategory'=>'Philtre Faves'),
    "Makeup"=>array('position'=>3,'subcategory'=>'Couple'),
    "Family"=>array('position'=>4,'subcategory'=>'Teen'),
    "Scent"=>array('position'=>5,'subcategory'=>''),
    "Gift & Travel"=>array('position'=>6,'subcategory'=>'')

);
$rootCategoryId = Mage::app()->getStore()->getRootCategoryId();

foreach($_topCategory as $name=>$data){
    $cat = Mage::getModel('catalog/category');
    $cat->setPath('1/' . $rootCategoryId)
        ->setName($name)
        ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
        ->setIsActive(1)
        ->setIncludeInMenu(1)
        ->setIsAnchor(1)
        ->setPosition($data['position'])
        ->save();

    if($data['subcategory']!=''){
        $subcat = Mage::getModel('catalog/category');
        $subcat->setPath($cat->getPath())
            ->setName($data['subcategory'])
            ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
            ->setIsActive(1)
            ->setIncludeInMenu(1)
            ->setIsAnchor(1)
            ->save();
    }
}

$installer->endSetup();