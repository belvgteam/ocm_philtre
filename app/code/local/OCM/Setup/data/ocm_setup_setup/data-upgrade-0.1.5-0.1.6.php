<?php

try {

        $installer = $this;
        $installer->startSetup();
        $installer->addAttribute('catalog_product', 'brand', array(
            'group' => 'General',
            'type' => 'int',
            'backend' => '',
            'frontend' => '',
            'label' => 'Brand',
            'input' => 'select',
            'class' => '',
            'source' => 'eav/entity_attribute_source_table',
            'is_global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
            'visible' => true,
            'required' => true,
            'user_defined' => true,
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'apply_to' => 'simple',
            'unique' => false,
            'is_configurable' => true,
            'option' => array('value' => array(
                'type1' => array('Baggu'),
                'type2' => array('Alima pure'),
                'type3' => array('Scotch'),
                'type4' => array('Chocalte Sun'),
                'type5' => array("Sir Richard's Condon company"),
                'type6' => array("Pande"),
                'type7' => array('Athletic'),
                'type8' => array('Verbena'),
                )
            ),
        ));


    $homeContent = <<<EOD
<div class="home-page-content">
<div class="box-product best-selling-product">{{block type="core/template" template="catalog/category/special-category.phtml" name="home.bestseller"}}</div>
<div class="box-product list-brand">{{block type="core/template" template="page/html/list.brand.phtml" name="home.listbrand"}}</div>
<div class="box-product feature-product">{{block type="featuredproducts/product_list" template="catalog/product/feature_product.phtml" name="home.featureproduct"}}</div>
</div>
EOD;

    $page = Mage::getModel('cms/page')->load('home');
    if($page->getId()){
        $page->setContent($homeContent);
        $page->setRootTemplate('one_column')->save();
    }



    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}