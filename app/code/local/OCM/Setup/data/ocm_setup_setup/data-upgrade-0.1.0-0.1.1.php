<?php

try {

    $installer = $this;
    $installer->startSetup();
    $config = new Mage_Core_Model_Config();
    $config->saveConfig('design/package/name', 'philtre' , 'default', 0);
    $config->saveConfig('design/package/default', 'default' , 'default', 0);
    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}