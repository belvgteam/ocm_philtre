<?php
class OCM_Setup_Helper_Data extends Mage_Core_Helper_Abstract {
    function getSubcategories(){
        $categories = Mage::helper('catalog/category')->getStoreCategories(false,true,false)
            ->addAttributeToFilter('level',array('gt'=>2))
            ->addAttributeToFilter('is_active',1)
            ->addAttributeToFilter('include_in_menu',1)
            ->setOrder('position','asc')
        ;
        return $categories;
    }

    public function getAttributeOptions($arg_attribute)
    {
        $attribute_model        = Mage::getModel('eav/entity_attribute');
        $attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;
        $attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
        $attribute              = $attribute_model->load($attribute_code);
        $attribute_table        = $attribute_options_model->setAttribute($attribute);
        $options                = $attribute_options_model->getAllOptions(false);
        return $options;
    }

    public function formatBrandKey($str)
    {
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', Mage::helper('catalog/product_url')->format($str));
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');

        return $urlKey;
    }
}