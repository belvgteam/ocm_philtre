<?php

class OCM_Catalog_Model_Observer
{
    function setColorPosition($observer)
    {
        //get attribute is color
        $attribute_model        = Mage::getModel('eav/entity_attribute');
        $attribute_code         = $attribute_model->getIdByCode('catalog_product', 'color');
        $attribute              = $attribute_model->load($attribute_code);
        $colorId = $attribute->getId();
        //get product
        $product = $observer->getProduct();
        $resource = Mage::getSingleton('core/resource');
        $table = $resource->getTableName('catalog/product_super_attribute');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $query = 'SELECT product_super_attribute_id FROM ' . $table . ' WHERE product_id = ' . (int)$product->getId() . ' and attribute_id = '. $colorId;
        $id = $readConnection->fetchOne($query);
        // set position is 100
        if(isset($id) && $id!=''){
            $query = "UPDATE {$table} SET position = '100' WHERE product_super_attribute_id = " . (int)$id;
            $writeConnection->query($query);
        }
    }
}