<?php

class Intell_Colorswatch_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getImageByProduct($productId)
    {
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'color');
        $product = Mage::getModel('catalog/product')->load($productId);
        $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
        $imageHtml = '';
        $mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        foreach ($productAttributeOptions as $productAttribute) {
            if($productAttribute['attribute_id']==$attributeModel->getId()){
                foreach ($productAttribute['values'] as $attribute) {
                    //swatch images
                    $data = Mage::getModel('colorswatch/colorswatch')->getCollection()->addFieldToFilter('option_id', $attribute['value_index'])->getFirstItem()->getData();
                    //resize images
                    $_imageUrl = Mage::getBaseDir("media").DS.$data['filename'];
                    $imageResized = Mage::getBaseDir("media").DS."resized".DS.$data['filename'];
                    if (!file_exists($imageResized)&&file_exists($_imageUrl)) :
                        $imageObj = new Varien_Image($_imageUrl);
                        $imageObj->constrainOnly(TRUE);
                        $imageObj->keepAspectRatio(TRUE);
                        $imageObj->keepFrame(FALSE);
                        $imageObj->resize(30, 30);
                        $imageObj->save($imageResized);
                    endif;
                    $imageHtml .= "<img alt='".$attribute['store_label']."' title='".$attribute['store_label']."' src='".$mediaUrl."resized/".$data['filename']."'>";
                }
                break;
            }
        }
        return $imageHtml;
    }
}