<?php
class Intell_Colorswatch_Block_Colorswatch extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getColorswatch()     
     { 
        if (!$this->hasData('colorswatch')) {
            $this->setData('colorswatch', Mage::registry('colorswatch'));
        }
        return $this->getData('colorswatch');
        
    }
	
	public function getSwatchImages(){
		// Collect options applicable to the configurable product
		$product = $this->getProduct();
		$productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
		
		//get associated products
		$associatedProductIds = $product->getTypeInstance(true)->getUsedProductIds($product);
		$associatedProducts = array();
		foreach($associatedProductIds as $associatedProductId){
			$clildProduct = Mage::getModel('catalog/product')->load($associatedProductId);
            if($clildProduct->getImage()){
                $associatedProducts[$clildProduct->getColor()] = array('image'=>$clildProduct->getImageUrl(),'productid'=>$clildProduct->getId());
            } elseif($clildProduct->getSmallImageUrl()) {
                $associatedProducts[$clildProduct->getColor()] = array('image'=>$clildProduct->getSmallImageUrl(345,345),'productid'=>$clildProduct->getId());
            } else {
                $associatedProducts[$clildProduct->getColor()] = array('image'=>$clildProduct->getThumbnailUrl(345,345),'productid'=>$clildProduct->getId());
            }
		}

		$swatch = "<ul>";
		$mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

		foreach ($productAttributeOptions as $productAttribute) {
            if($productAttribute['attribute_id']==$this->getAttributeId()){
                foreach ($productAttribute['values'] as $attribute) {
                    //swatch images
                        $data = Mage::getModel('colorswatch/colorswatch')->getCollection()->addFieldToFilter('option_id', $attribute['value_index'])->getData();
                        $imageHtml = "<img alt='".$attribute['store_label']."' title='".$attribute['store_label']."' width='30' height='30' src='".$mediaUrl.$data[0]['filename']."'>";
                        $sekId = '"'.$product->getId().'"';
                        $productImageUrl = '"'.$associatedProducts[$attribute['value_index']]['image'].'"';
                        $text = "<a href='javascript:void(0)' class='color-".$attribute['value_index']."' title='".$attribute['store_label']."' onclick='changeColorDropdown".$product->getId()."(jQuery(this),".$attribute['value_index'].",".$sekId.",".$productImageUrl.",".'"'.$this->getAttributeId().'"'.");'>".$imageHtml;
                        $text .= "</a>";

                    $swatch .= "<li> {$text} </li>";
                }
                break;
            }
		}
		$swatch .= "</ul>";
		
		//print_r($product->getFinalPrice());
		//print_r($productAttributeOptions);die;
		
		return $swatch;
	}
}