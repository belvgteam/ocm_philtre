<?php
/**
 * Insert value for swatches 
 *
 * @author Khennt

 * @category   Intell
 * @package    Intell_Colorswatch
 * @copyright  Copyright (c) 2010 Guidance Solutions (http://www.guidance.com)
 *
 */


try{


	$installer = $this;
	$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
	$installer->startSetup();
	
	// Get attribute info
	$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
	->setCodeFilter('color')
	->getFirstItem();

	// Add the attribute code here.
	$attribute = Mage::getModel('catalog/product')->getResource()->getAttribute("color");

	// Checking if the attribute is either select or multiselect type.
	if($attribute->usesSource()){
		// Getting all the sources (options) and print as label-value pair
		$options = $attribute->getSource()->getAllOptions(false);
	}

	foreach ($options as $item) {
		$model = Mage::getModel('colorswatch/colorswatch');
		$sel = $model->getCollection()->addFieldToFilter('option_id', $item['value'])->getData();
		if(empty($sel)){
			$data = array("title" => $item['label'], "content" => "Logo of " . $item['label'], "option_id" => $item['value'], "filename" => "CS_" . $item['label'] . '.png');
			$model->setData($data);
			try {
				$model->save();
			} catch (Exception $e) {
				Mage::logException($e);
				Mage::log("ERROR IN SETUP ".$e->getMessage());
			}
		}		
	}	
	
	$installer->endSetup();


}catch(Excpetion $e){
	Mage::logException($e);
	Mage::log("ERROR IN SETUP ".$e->getMessage());


}
